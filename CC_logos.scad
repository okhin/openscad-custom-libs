// A tiny CC 0 logo.
use <okhin.scad>;

module cc_domain_public(s=12) {
    hollow_circle(d=s, e=0.5);
    text("0", halign="center", valign="center", size=s-1, font="Inconsolata");
    difference() {
        translate([s*1.2, 0,])
            square([s*2, s], center=true);
        circle(d=s+2);
        translate([2 * s / 3, 0]) {
            translate([0, s/12])
                text("PUBLIC", halign="left", valign="bottom", size=s/4, font="Arial:bold" );
            translate([0, -s/12])
                text("DOMAIN", halign="left", valign="top", size=s/4, font="Arial:bold" );
        };
    };
};
